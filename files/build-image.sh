#!/bin/bash

#==============================================================================
# Can pass in:
# arg[1] = "clean" Will first remove an existing docker image then make a new
#                  new image.
#==============================================================================

IMAGENAME=swdev:buildbox
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo 'This may take some time to build the image . . .'
	
if [ "$1" == "clean" ]; then
    echo -e "\nINFO: Removing old images of: $IMAGENAME\n"
    docker rmi --force $IMAGENAME
fi

GIT_CONFIG=~/.gitconfig
if test -f "$GIT_CONFIG"; then
    cp $GIT_CONFIG ./
else
    # This can cause git problems in the container if you intend
    # to ever do a "git commit".  You will be forced to:
    # git config --global user.email "john@acme.com"
    # git config --global user.name "Example Name"
    echo "WARNING: ~/.gitconfig not found"
fi

CACHE=""
if [ "$1" == "clean" ]; then
    CACHE="--no-cache"
fi

echo -e "\nINFO: Building image: $IMAGENAME\n"

docker build $CACHE -t $IMAGENAME --network host \
    -f ${SCRIPT_DIR}/Dockerfile \
    --build-arg BUILDBOX_USER=swdev \
    ${SCRIPT_DIR}

if [ $? -eq 0 ]; then 
    echo -e "\n**************************************************************"
    echo -e "INFO: Completed building image: $IMAGENAME"
    echo -e "**************************************************************\n\n"
else
    echo -e "\n**************************************************************"
    echo -e "ERROR: Failed to build image: $IMAGENAME"
    echo -e "**************************************************************\n\n"
fi
