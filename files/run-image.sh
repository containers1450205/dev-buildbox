#!/bin/bash

IMAGENAME=swdev:buildbox

WORKDIR=${PWD}
if [ "$#" -gt 0 ]; then
    WORKDIR=${1}
fi

# Make sure your ssh keys are loaded so the container
# has access to them through SSH_AUTH_SOCK.
ssh-add

docker run -ti --rm=true \
    -e "MHF_HOST_UID=$(id -u)" \
    -e "MHF_HOST_GID=$(id -g)" \
    -e DISPLAY=$DISPLAY \
    -e force_color_prompt=yes \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $SSH_AUTH_SOCK:/tmp/ssh.socket \
    -e SSH_AUTH_SOCK=/tmp/ssh.socket \
    -v ${WORKDIR}:/workdir \
    --net=host \
    --workdir=/workdir \
    $IMAGENAME \
