# Software Development Container for Node.js, C/C++, Rust, and Python 

This container can be used as a *buildbox* to develop applications in several development languages. Currently, the buildbox supports the following languages:

* C/C++ (GCC/G++ 9,11,12*)
    - Select via update-alternatives. Default is GCC/G++ 12
* Clang (v16)
* Rust (v1.67.0)
* Node.js (v18.13.0)
* Python (v3.10.9)
* Bazel (v5.1.1)
* Ninja
* Meson

The buildbox fully supports VS-Code development inside the container by allowing VS-Code to inject a server into the container and then open it. The user (*swdev*) inside the container shares the same UID/GID as the host user outside the container. Files created by *swdev* in a mounted directory will have the same owner as the host which make working with files from inside/outside the container much easier.

## Usage

### Build Container

To build the container go into the top-level project directory and run:

```
./build.sh
```

### Run Container

To run the container execute the following script with an optional local directory to mount inside the container. If no directory is specified then the current working directory is mounted.

```
./run.sh [optional_local_path_to_mount]
```

## Configuring VS-Code for the Container

VS-Code can open a project in a Docker container if the top-level folder has a `.devcontainer.json` file that describes the container and the extensions to load into it. For example, the configuration files for the supported languages follow:

### C++ VS-Code Configuration
```json
{
  "image": "swdev:buildbox",
  "runArgs": [
    "--rm=true",
    "-e \"MHF_HOST_UID=$(id -u)\"",
    "-e \"MHF_HOST_GID=$(id -g)\"",
    "--net=host",
    "--cap-add=SYS_PTRACE",
    "--security-opt",
    "seccomp=unconfined"
  ],
  "customizations": {
    "vscode": {
      "extensions": [
        "ms-vscode.cpptools-extension-pack"
      ]
    }
  },
  "containerUser": "swdev",
  "workspaceMount": "source=${localWorkspaceFolder},target=/workdir,type=bind",
  "workspaceFolder": "/workdir"
}
```

### Rust VS-Code Configuration
```json
{
  "image": "swdev:buildbox",
  "runArgs": [
    "--rm=true",
    "-e \"MHF_HOST_UID=$(id -u)\"",
    "-e \"MHF_HOST_GID=$(id -g)\"",
    "--net=host",
    "--cap-add=SYS_PTRACE",
    "--security-opt",
    "seccomp=unconfined"
  ],
  "customizations": {
    "vscode": {
      "extensions": [
        "vadimcn.vscode-lldb",
        "rust-lang.rust-analyzer",
        "serayuzgur.crates",
        "bungcip.better-toml"
      ]
    }
  },
  "containerUser": "swdev",
  "workspaceMount": "source=${localWorkspaceFolder},target=/workdir,type=bind",
  "workspaceFolder": "/workdir"
}
```

### Node.js VS-Code Configuration
```json
{
  "image": "swdev:buildbox",
  "runArgs": [
    "--rm=true",
    "-e \"MHF_HOST_UID=$(id -u)\"",
    "-e \"MHF_HOST_GID=$(id -g)\"",
    "--net=host",
    "--cap-add=SYS_PTRACE",
    "--security-opt",
    "seccomp=unconfined"
  ],
  "customizations": {
    "vscode": {
      "extensions": [
      ]
    }
  },
  "containerUser": "swdev",
  "workspaceMount": "source=${localWorkspaceFolder},target=/workdir,type=bind",
  "workspaceFolder": "/workdir"
}
```

### Python VS-Code Configuration
```json
{
  "image": "swdev:buildbox",
  "runArgs": [
    "--rm=true",
    "-e \"MHF_HOST_UID=$(id -u)\"",
    "-e \"MHF_HOST_GID=$(id -g)\"",
    "--net=host",
    "--cap-add=SYS_PTRACE",
    "--security-opt",
    "seccomp=unconfined"
  ],
  "customizations": {
    "vscode": {
      "extensions": [
        "ms-python.python"
      ]
    }
  },
  "containerUser": "swdev",
  "workspaceMount": "source=${localWorkspaceFolder},target=/workdir,type=bind",
  "workspaceFolder": "/workdir"
}
```

Additional details on how to use containers with VS-Code can be found [here](https://code.visualstudio.com/docs/devcontainers/containers).